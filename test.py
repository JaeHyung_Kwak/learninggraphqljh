import unittest
import json

from pprint import pprint

from schema import schema, db_session


class ActivityBookingsCancelTest(unittest.TestCase):

    def test_authors_all(self):

        query = '''
            query testAll {
              authors {
                _id
                name,
                twitterHandle
              }
            }
        '''
        expected = {'authors': [{'_id': 'arunoda',
              'name': 'Arunoda Susiripala',
              'twitterHandle': '@arunoda'},
             {'_id': 'pahan',
              'name': 'Pahan Sarathchandra',
              'twitterHandle': '@pahans'},
             {'_id': 'indi', 'name': 'Kasun Indi', 'twitterHandle': '@indi'}]}


        result = schema.execute(query, context_value={'session': db_session})


        self.assertEqual(
            json.dumps(result.data),
            json.dumps(expected)
        )

    def test_authors_name_all(self):

        query = '''
            {
              authors {
                name
              }
            }
        '''
        expected = {
            "authors": [{
                "name": "Arunoda Susiripala"
            }, {
                "name": "Pahan Sarathchandra"
            }, {"name": "Kasun Indi"}]
        }

        result = schema.execute(query, context_value={'session': db_session})
        self.assertEqual(
            json.dumps(result.data),
            json.dumps(expected)
        )

    def test_author(self):
        query = '''
            {
              author(_id: "arunoda") {
                _id
                name,
                twitterHandle
              }
            }
        '''
        expected = {'author': {'_id': 'arunoda',
            'name': 'Arunoda Susiripala',
            'twitterHandle': '@arunoda'}}
        result = schema.execute(query, context_value={'session': db_session})
        # print(json.loads(json.dumps(result.data)))
        self.assertEqual(
            json.dumps(result.data),
            json.dumps(expected)
        )

    def test_posts(self):
        query = '''{
                  posts {
                    _id
                    title
                    category
                    comments {
                      _id
                      content
                    }
                    author {
                      _id
                      name
                    }
                  }
                }
                '''
        expected = {'posts': [{'_id': '0176413761b289e6d64c2c14a758c1c7',
            'author': {'_id': 'arunoda'},
            'category': 'meteor',
            'comments': [{'_id': 'cid-64013942',
                          'content': 'This is a very good blog post'},
                         {'_id': 'cid-35236174',
                          'content': 'Keep up the good work'}],
            'title': 'Sharing the Meteor Login State Between Subdomains'},
           {'_id': '03390abb5570ce03ae524397d215713b',
            'author': {'_id': 'pahan'},
            'category': 'product',
            'comments': [{'_id': 'cid-64013942',
                          'content': 'This is a very good blog post'},
                         {'_id': 'cid-35236174',
                          'content': 'Keep up the good work'}],
            'title': 'New Feature: Tracking Error Status with Kadira'},
           {'_id': '0be4bea0330ccb5ecf781a9f69a64bc8',
            'author': {'_id': 'indi'},
            'category': 'product',
            'comments': [{'_id': 'cid-64013942',
                          'content': 'This is a very good blog post'},
                         {'_id': 'cid-35236174',
                          'content': 'Keep up the good work'}],
            'title': 'What Should Kadira Build Next?'},
           {'_id': '19085291c89f0d04943093c4ff16b664',
            'author': {'_id': 'indi'},
            'category': 'product',
            'comments': [{'_id': 'cid-64013942',
                          'content': 'This is a very good blog post'},
                         {'_id': 'cid-35236174',
                          'content': 'Keep up the good work'}],
            'title': 'Awesome Error Tracking Solution for Meteor Apps with '
                     'Kadira'},
           {'_id': '1afff9dfb0b97b5882c72cb60844e034',
            'author': {'_id': 'indi'},
            'category': 'product',
            'comments': [{'_id': 'cid-64013942',
                          'content': 'This is a very good blog post'},
                         {'_id': 'cid-35236174',
                          'content': 'Keep up the good work'}],
            'title': 'Tracking Meteor CPU Usage with Kadira'},
           {'_id': '1bd16dfab1de982317d2ba4382ec8c86',
            'author': {'_id': 'indi'},
            'category': 'meteor',
            'comments': [{'_id': 'cid-64013942',
                          'content': 'This is a very good blog post'},
                         {'_id': 'cid-35236174',
                          'content': 'Keep up the good work'}],
            'title': 'Meteor Server Side Rendering Support with FlowRouter and '
                     'React'},
           {'_id': '285292901bb38be8f57dd2885c517826',
            'author': {'_id': 'indi'},
            'category': 'user-story',
            'comments': [{'_id': 'cid-64013942',
                          'content': 'This is a very good blog post'},
                         {'_id': 'cid-35236174',
                          'content': 'Keep up the good work'}],
            'title': 'How Brent is using Kadira with his development workflow'},
           {'_id': '2f6b59fd0b182dc6e2f0051696c70d70',
            'author': {'_id': 'indi'},
            'category': 'other',
            'comments': [{'_id': 'cid-64013942',
                          'content': 'This is a very good blog post'},
                         {'_id': 'cid-35236174',
                          'content': 'Keep up the good work'}],
            'title': 'Understanding Mean, Histogram and Percentiles'},
           {'_id': '3d7a3853bf435c0f00e46e15257a94d9',
            'author': {'_id': 'indi'},
            'category': 'product',
            'comments': [{'_id': 'cid-64013942',
                          'content': 'This is a very good blog post'},
                         {'_id': 'cid-35236174',
                          'content': 'Keep up the good work'}],
            'title': 'Introducing Kadira Debug, Version 2'}]}

        result = schema.execute(query, context_value={'session': db_session})

        print(result.data)
        # self.assertEqual(
        #     json.dumps(result.data, sort_keys=True),
        #     json.dumps(expected, sort_keys=True)
        # )

    def test_posts_with_category_arg(self):
        query = '''{
              posts(category:"meteor") {
                _id
                title
                category
              }
            }
        '''
        expected = {'posts': [{'_id': '0176413761b289e6d64c2c14a758c1c7',
            'category': 'meteor',
            'title': 'Sharing the Meteor Login State Between Subdomains'},
           {'_id': '1bd16dfab1de982317d2ba4382ec8c86',
            'category': 'meteor',
            'title': 'Meteor Server Side Rendering Support with FlowRouter and '
                     'React'}]}

        result = schema.execute(query, context_value={'session': db_session})

        self.assertEqual(
            json.dumps(result.data, sort_keys=True),
            json.dumps(expected, sort_keys=True)
        )

    def test_latest_post(self):
        query = '''{
          latestPost {
            _id,
            title
          }
        }'''

        result = schema.execute(query, context_value={'session': db_session})

        print(result.data)


    def test_filterPosts(self):
        query = '''{
          readPosts(selector: {filter: "post._id=='0176413761b289e6d64c2c14a758c1c7'"}) {
            _id,
            title
            titleEN
          }
        }'''
        # print(schema)
        result = schema.execute(query, context_value={'session': db_session})

        print(result.data)

    def test_filterComplex1(self):
        query = '''{
          readPosts(selector: {filter: "_id=='0176413761b289e6d64c2c14a758c1c7'"}) {
            _id,
            title
          }
        }'''

        result = schema.execute(query, context_value={'session': db_session})

        print(result.data)

    def test_createPost(self):
        query = ''' mutation a {
          createPost(postData: {_id: "0176413761b289e6d64c2c14a758c1c9", title: "abc"}) {
            post{
                _id,
                title
            }
          }
        }'''

        result = schema.execute(query, context_value={'session': db_session})
        # print(schema)
        print(result.data)

    def test_deletePost(self):
        query = ''' mutation a {
          deletePost(selector: {filter: {_id: "0176413761b289e6d64c2c14a758c1c7"}}) {
            post{
                _id,
                title
            }
          }
        }'''

        result = schema.execute(query, context_value={'session': db_session})
        print(schema)
        print(result.data)

        pass

if __name__ == '__main__':
    unittest.main()
